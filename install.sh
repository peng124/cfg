#!/bin/bash

if [ -d $HOME/.emacs.d ]; then
   echo "~/.emacs.d exists, please back it up and remove it"
   exit
fi

git clone https://peng124@bitbucket.org/peng124/spacemacs.git ~/.emacs.d
ln ~/.emacs.d/.spacemacs ~/.spacemacs   
